// const logger = require("../../controller/logger");

if (user) 
    console.log(user);

if (SERVER_HOST)
    console.log(SERVER_HOST);

const GRAPH_APPEND_LIMIT = 100;
var socket = null;
let active_device = null;
var device_list_get = null;
var device_sensor_data = {};
var myChartRedRaw;
var myChartIRRaw;
var myChartBPM;
var myChartspO2;
var myChartRedFiltered;
var myChartIRFiltered;
var realTimeData;

var chartOptRedRaw = {
    type: 'line',
    // data: {
    //     // labels: [new Date("2015-3-15 13:3").toLocaleString(), new Date("2015-3-25 13:2").toLocaleString(), new Date("2015-4-25 14:12").toLocaleString()],
    //     datasets: [{
    //     label: 'IR',
    //     data: [],
    //     backgroundColor: [
    //         'rgba(255, 99, 132, 0.2)',
    //         'rgba(54, 162, 235, 0.2)',
    //         'rgba(255, 206, 86, 0.2)',
    //         'rgba(75, 192, 192, 0.2)',
    //         'rgba(153, 102, 255, 0.2)',
    //         'rgba(255, 159, 64, 0.2)'
    //     ],
    //     borderColor: [
    //         'rgba(255, 99, 132, 1)',
    //         'rgba(54, 162, 235, 1)',
    //         'rgba(255, 206, 86, 1)',
    //         'rgba(75, 192, 192, 1)',
    //         'rgba(153, 102, 255, 1)',
    //         'rgba(255, 159, 64, 1)'
    //     ],
    //     borderWidth: 1
    //     }]
    // },
    options: {
        elements: {
            line: {
                tension: 0 // disables bezier curves
            }
        },
        animation: {
            duration: 0, // general animation time
        },
        hover: {
            animationDuration: 0, // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0, // animation duration after a resize
        scales: {
            xAxes: [{
                    type: 'time',
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 20,
                        // fontSize: 8
                    },
                    time: {
                        parser: 'DD-MM-YYYY HH:mm:ss.SSS',
                        // round: 'day'                                                                                                                                                                            
                        tooltipFormat: 'DD-MM-YYYY HH:mm:ss.SSS',
                        displayFormats: {
                            millisecond: 'DD-MM HH:mm:ss',//'DD-MM-YYYY HH:mm:ss.SSS',
                            second: 'DD-MM HH:mm:ss',//'DD-MM-YYYY HH:mm:ss',
                            minute: 'DD-MM HH:mm:ss',//'DD-MM-YYYY HH:mm',
                            hour: 'DD-MM HH:mm:ss'//'DD-MM-YYYY HH'
                        }
                    },
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
            }]//,
            // onAnimationComplete: function () {
            //     var sourceCanvas = this.chart.ctx.canvas;
            //     // the -5 is so that we don't copy the edges of the line
            //     var copyWidth = this.scale.xScalePaddingLeft - 5;
            //     // the +5 is so that the bottommost y axis label is not clipped off
            //     // we could factor this in using measureText if we wanted to be generic
            //     var copyHeight = this.scale.endPoint + 5;
            //     var targetCtx = document.getElementById("myChartAxis").getContext("2d");
            //     targetCtx.canvas.width = copyWidth;
            //     targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            // }
        },
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            zoom: {
                // // // Container for pan options
                // pan: {
                //     // Boolean to enable panning
                //     enabled: true,

                //     // Panning directions. Remove the appropriate direction to disable
                //     // Eg. 'y' would only allow panning in the y direction
                //     // A function that is called as the user is panning and returns the
                //     // available directions can also be used:
                //     //   mode: function({ chart }) {
                //     //     return 'xy';
                //     //   },
                //     mode: 'xy',

                //     rangeMin: {
                //         // Format of min pan range depends on scale type
                //         x: null,
                //         y: null
                //     },
                //     rangeMax: {
                //         // Format of max pan range depends on scale type
                //         x: null,
                //         y: null
                //     },

                //     // On category scale, factor of pan velocity
                //     speed: 20,

                //     // Minimal pan distance required before actually applying pan
                //     threshold: 10,

                //     // // Function called while the user is panning
                //     // onPan: function({chart}) { console.log(`I'm panning!!!`); },
                //     // // Function called once panning is completed
                //     // onPanComplete: function({chart}) { console.log(`I was panned!!!`); }
                // },
        
                // Container for zoom options
                zoom: {
                    // Boolean to enable zooming
                    enabled: true,
        
                    // Enable drag-to-zoom behavior
                    drag: true,
        
                    // Drag-to-zoom effect can be customized
                    drag: {
                        borderColor: 'rgba(225,255,255,0.3)',
                        borderWidth: 5,
                        backgroundColor: 'rgb(225,255,255,0.3)',
                        animationDuration: 0.2
                    },
        
                    // Zooming directions. Remove the appropriate direction to disable
                    // Eg. 'y' would only allow zooming in the y direction
                    // A function that is called as the user is zooming and returns the
                    // available directions can also be used:
                    //   mode: function({ chart }) {
                    //     return 'xy';
                    //   },
                    mode: 'xy',
        
                    rangeMin: {
                        // Format of min zoom range depends on scale type
                        x: null,
                        y: null
                    },
                    rangeMax: {
                        // Format of max zoom range depends on scale type
                        x: null,
                        y: null
                    },
        
                    // Speed of zoom via mouse wheel
                    // (percentage of zoom on a wheel event)
                    speed: 0.1,
        
                    // Minimal zoom distance required before actually applying zoom
                    threshold: 2,
        
                    // On category scale, minimal zoom level before actually applying zoom
                    sensitivity: 3,
        
                    // // Function called while the user is zooming
                    // onZoom: function({chart}) { console.log(`I'm zooming!!!`); },
                    // // Function called once zooming is completed
                    // onZoomComplete: function({chart}) { console.log(`I was zoomed!!!`); }
                }
            }
        }
    }
}

var chartOptIRRaw = {...chartOptRedRaw};

var chartOptBPM = {...chartOptRedRaw};

var chartOptspO2 = {...chartOptRedRaw};

var chartOptRedPulse = {...chartOptRedRaw};

var chartOptIRPulse = {...chartOptRedRaw};

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


// function emitEvent(){
//     console.log('Hello world');
//     socket.emit('request time', '3c8f06ad-75df-4d734-84d8-898938eb4aa4');
// }
// emitEvent();

$('#addDeviceUuidForm, #removeDeviceUuidForm').submit(e => {
    e.preventDefault();
    var form = $(e.target);
    var url = form.attr('action');
    var request_method = form.attr("method");
    var form_data = form.serialize(); 
    //var test = $("#sign-in-form").attr("action")
    //- $(".loaderSpinner").addClass("loaderSpinnerInline");
    $.ajax({
        type: request_method,
        url: url,
        data: form_data, // serializes the form's elements.
        beforeSend: function() {
            form.find('.fa-plus-square').css("display", "none");
            $(".loaderSpinner").addClass("loaderSpinnerInline");
        },
        success: data => {
            //- $(".loaderDiv").hide();
            //console.log(response)
            // console.log(data);
            alert(data.msg);
            if (data.redirect) 
                window.location.href = data.redirect;

            populateDeviceSubmenuSidebar(data.updated_device_list);
            $(".loaderSpinner").removeClass("loaderSpinnerInline");
            form.find('.fa-plus-square').css("display", "inline-block");
            //- new Promise(() => );
            
        },
        error: data => {
            $(".loaderSpinner").removeClass("loaderSpinnerInline");
            form.find('.fa-plus-square').css("display", "inline-block");
            alert(data.responseJSON.msg);
        }
    });
});
//$('#sidebarWrapper').addClass('hide-sidebar');
$('#sidebarButton, #dismissSidebar').on('click', () => {
    $('#sidebarWrapper').toggleClass('show-sidebar');
    $('#contentWrapper').toggleClass('show-sidebar');
})


// if($('#deviceSubmenu').children().length > 0){
//     $('#deviceSubmenuHeader').addClass('dropdown-toggle'); 
//     $('#deviceSubmenuHeader').attr("data-toggle", "collapse");
// }
// $('.dropdown-toggle').on('[aria-expanded="true"]', e=>{
//     $(event.target).find()
//     console.log('test')
// })

// //this is to avoid the menu from closing if clicked inside the menu 
// $('body').on("click", ".dropdown-menu", function(e) {
//     $(this).parent().is(".open") && e.stopPropagation();
// });


$('input[name="datetimes"]').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
        format: 'DD/MM/YYYY hh:mm A'
    }
});

(async () => {
    response = await fetch('/getDeviceInfo', {method: "POST"});
    device_list_get = await response.json();
    console.log(device_list_get);
    populateDeviceSubmenuSidebar(device_list_get);
})();

function populateDeviceSubmenuSidebar(device_list){
    if(!device_list) return;
    var here = $('#deviceSubmenu');
    here.empty();
    $('#deviceSubmenuHeader').addClass('dropdown-toggle');
    $('#deviceSubmenuHeader').attr("data-toggle", "collapse");
    for (i = 0; i < device_list.length; i++){
        new_link_device = $('<a href="javascript:void(0)" class="device_link"></a>');
        new_link_device_icon = $('<i class="fas fa-microchip" style="margin-right:5px"></i>');
        new_link_device_removeBtnWrapper = $('<div class="removeBtnDevice"></div>');
        new_link_device_removeBtnWrapperIcon = $('<i class="fas fa-minus-circle"></i>');
        new_link_device_removeBtnWrapper.append(new_link_device_removeBtnWrapperIcon);
        new_link_device.append(new_link_device_icon);
        new_link_device.append(device_list[i].Device_uuid);
        new_link_device.append(new_link_device_removeBtnWrapper);
        if (device_list[i].Device_uuid == active_device) new_link_device.addClass('active');
        new_link_device.attr('id', 'device_' + device_list[i].Device_uuid);
        new_submenu = $("<li></li>").append(new_link_device);
        here.append(new_submenu);
    }
}

async function populateDeviceInfo(device_info){
    if(!device_info) return;
    $('#deviceTabHeader a[aria-controls="tab-01"]').trigger('click');
    // $('#deviceTabHeader a[aria-controls="tab-01"] svg').removeClass('fa-info-circle');
    // $('#deviceTabHeader a[aria-controls="tab-01"] svg').addClass('fa-spinner fa-spin');
    response_promise = fetch('/getDataStream', {
        method: "POST",
        body: new URLSearchParams("device_uuid=" + device_info[0].Device_uuid + "&" + "get_all=true&count_data=true")
    });
    var here = $('#deviceInfo');
    // here.empty();
    if (!$('.infoCard#deviceCard' + device_info[0].Device_uuid).length){
        here.append($("<div>", {"class": "infoCard", "id": "deviceCard" + device_info[0].Device_uuid}));
        // Construct
        deviceInfo = $('.infoCard#deviceCard' + device_info[0].Device_uuid);
        deviceInfo.html('' +
        '<div class="deviceInfoHeader bg-primary" style="padding: 5px 10px; color:white; display: flex">' +
            '<div class="rounded mr-2" style="display:inline-flex; align-items:center"><i class="fas fa-info-circle style="vertical-align:middle;"></i></div>' +
            '<div class="deviceInfoName strong mr-auto" style="display:flex; align-items:center">Real time info</div><button class="btn" id="reloadDeviceInfoBtn' + device_info[0].Device_uuid + '" type="button" style="color:white"><i class="fas fa-sync-alt" id="deviceInfoSpinner' + active_device + '"></i></button></div>' +
        '<div class="toast-body">Real time measument will appear here</div>'
        )
        chosen_device = device_list_get.filter(data => {
            return data.Device_uuid == device_info[0].Device_uuid; 
        });
        $('#reloadDeviceInfoBtn' + device_info[0].Device_uuid).click(() => populateDeviceInfo(chosen_device))
    }
    here = $('#deviceInfo #deviceCard' + device_info[0].Device_uuid);
    here.on("activeDeviceChange", function () {
        if ($(this).attr("id") != 'deviceCard' + active_device)
            $(this).hide();
        else $(this).show();
    })
    name_section = $('#deviceInfo #deviceCard' + device_info[0].Device_uuid + ' .deviceInfoHeader .deviceInfoName');
    // name_section = $('<div>Device name: </div>');
    name_section.html(device_info[0].Device_name);
    uuid_section = $('#deviceInfo #deviceCard' + device_info[0].Device_uuid + ' .toast-body');
    uuid_section.html('Device uuid: ' + device_info[0].Device_uuid);
    
    // count_doc_section_countNum = $('<span class="countDocCountNum"></span>');
    // count_doc_section.append(count_doc_section_countNum);

    // here.append(name_section);
    // here.append(uuid_section);
    // here.append(count_doc_section);
    $('#deviceInfoSpinner' + device_info[0].Device_uuid).addClass('fa-spin');
    response = await response_promise;
    count_ret = await response.json();
    console.log(count_ret);
    $('#deviceInfoSpinner' + device_info[0].Device_uuid).removeClass('fa-spin');
    // console.log(count_ret);
    if (count_ret.query_info.Device_uuid == device_info[0].Device_uuid){
        uuid_section.append('<div>Total number of record stored on database: ' + count_ret.count_doc +'</div>');
    }
}



async function populateDeviceGraph(device_data=null, clear_all=true, append_limit=null){
    
    if (clear_all){
        redDataset.data.length = 0;
        redMADataset.data.length = 0;
        irDataset.data.length = 0;
        irMADataset.data.length = 0;
        spO2Dataset.data.length = 0;
        bpmDataset.data.length = 0;
        redPulseDataset.data.length = 0;
        irPulseDataset.data.length = 0;
    }
    if (device_data) {
        // if (!device_data.hasOwnProperty('return_data'))
        //     throw('Not valid device data'); 
        // var timeFormat = 'DD/MM/YYYY';

        if (append_limit && redDataset.data.length > append_limit){
            redDataset.data.splice(0,device_data.length);
            redMADataset.data.splice(0,device_data.length);
            irDataset.data.splice(0,device_data.length);
            irMADataset.data.splice(0,device_data.length);
            bpmDataset.data.splice(0,device_data.length);
            spO2Dataset.data.splice(0,device_data.length);
            redPulseDataset.data.splice(0,device_data.length);
            irPulseDataset.data.splice(0,device_data.length);
        }
        
        time_data = [];
        for (i = 0; i < device_data.length; i++){
            map_item = {};
            map_item["t"] = moment(new Date(device_data[i].serverTimestamp))
                            .local()
                            .format('DD-MM-YYYY HH:mm:ss.SSS')
            map_item["record"] = device_data[i];
            time_data.push(map_item);
        }
        // time_data = device_data.map(record  => {
        //     map_item = {};
        //     // map_item.t = moment(Date.parse(record.originTimestamp) + record.cpuTimestamp)
        //     //                 .local()
        //     //                 .format('DD-MM-YYYY HH:mm:ss.SSS')
        //     map_item["t"] = moment(new Date(record.serverTimestamp))
        //                     .local()
        //                     .format('DD-MM-YYYY HH:mm:ss.SSS')
        //     map_item["record"] = record;
        //     return map_item;
        // });
        // console.log(time_data);

        $('#resetZoom').click(() => {
            mapStringToChartObject[$('.chartAreaWrapper .tab-content .tab-pane.active canvas').attr('id')].resetZoom();
        })

        
        
        for (i = 0; i < time_data.length; i++){
            red_item = {};
            red_ma_item = {};
            ir_item = {};
            ir_ma_item = {};
            spo2_item = {};
            bpm_item = {};
            red_pulse_item = {};
            ir_pulse_item = {};

            red_item.t = time_data[i].t;
            red_ma_item.t = time_data[i].t;
            ir_item.t = time_data[i].t;
            ir_ma_item.t = time_data[i].t;
            spo2_item.t = time_data[i].t;
            bpm_item.t = time_data[i].t;
            red_pulse_item.t = time_data[i].t;
            ir_pulse_item.t = time_data[i].t;        

            red_item.y = time_data[i].record.Red;
            red_ma_item.y = time_data[i].record.RedMA;
            ir_item.y = time_data[i].record.IR;
            ir_ma_item.y = time_data[i].record.IRMA;
            spo2_item.y = time_data[i].record.spO2;
            bpm_item.y = time_data[i].record.BPM;
            red_pulse_item.y = time_data[i].record.RedFil;
            ir_pulse_item.y = time_data[i].record.IRFil;

            redDataset.data.push(red_item);
            redMADataset.data.push(red_ma_item);
            irDataset.data.push(ir_item);
            irMADataset.data.push(ir_ma_item);
            spO2Dataset.data.push(spo2_item);
            bpmDataset.data.push(bpm_item);
            redPulseDataset.data.push(red_pulse_item);
            irPulseDataset.data.push(ir_pulse_item);
            // detectPulseDataset.data.push(0);
        }
    }
    myChartRedRaw.update();
    myChartIRRaw.update();
    myChartBPM.update();
    myChartspO2.update();
    myChartRedPulse.update();
    myChartIRPulse.update();
}

function addDataToChart(chart, new_data){

}
function setActiveDeviceUUID(device_uuid){
    if (active_device != device_uuid){
        active_device = device_uuid;
        $('.dataTable').trigger('activeDeviceChange')
        $('.infoCard').trigger('activeDeviceChange')
    }
}
$('ul#deviceSubmenu').on('click', async function() {
    if(event.target.id.substr(7) == active_device) return;
    $('#device_' + active_device).removeClass('active');
    $('#customSwitch1').prop('checked', false);
    $('#customSwitch1').trigger("change");
    setActiveDeviceUUID(event.target.id.substr(7));
    $('#device_' + active_device).addClass('active');
    chosen_device = device_list_get.filter(data => {
        return data.Device_uuid == active_device; 
    });
    populateDeviceGraph(null, clear_all = true);
    populateDeviceInfo(chosen_device);
    // console.log();
    // if (active_device)
    // response = await fetch('/getDataStream', {
    //     method: "POST",
    //     body: new URLSearchParams("device_uuid=" + active_device + "&" + "index_start=30&index_end=100")
    // });
    // active_device_data = await response.json();
    // console.log(active_device_data);
    // populateDeviceSubmenuSidebar(device_list_get);

    // populateDeviceGraph(active_device_data);
})

function pathJoin(parts, sep){
    const separator = sep || '/';
    parts = parts.map((part, index)=>{
        if (index) {
            part = part.replace(new RegExp('^' + separator), '');
        }
        if (index !== parts.length - 1) {
            part = part.replace(new RegExp(separator + '$'), '');
        }
        return part;
    })
    return parts.join(separator);
}

// (async() => {
// socket = io.connect('remote-health-biometric.herokuapp.com')
// while(true){
//     socket.emit('test debug', 'hihi');
//     await timeout(1000);
// }
// })()
$('#customSwitch1').change(function() {
    if(this.checked){
        //Do stuff
        destroyGraphData();
        if ( !socket ) {   
            socket = io.connect(pathJoin([SERVER_HOST, '/subscribe_service']));
            // socket = io.connect(pathJoin(['remote-health-biometric.herokuapp.com', '/subscribe_service']));

            // socket.on('connect', function(){console.log('connected')});                                 
            // socket.on('disconnect', function (){console.log('disconnected')});
        } else {
            socket.connect(); // Yep, socket.socket ( 2 times )
        }
        $('#graphDatasheetBtn').hide();
        $('#realTimeInfo').toast('show');
        populateDeviceGraph(null, clear_all=true);
        socket.emit('subscribe to', active_device);
        socket.on('device has new data', (device_uuid, realTimeData) => {
            // console.log(realTimeData);
            $('#realTimeSpinner').removeClass('fa-info-circle').addClass('fa-spinner fa-spin');
            populateDeviceGraph(realTimeData, clear_all=false, append_limit=GRAPH_APPEND_LIMIT);
            
            // $('#realTimeInfoPaneRedIR').empty();
            // $('#realTimeInfoPaneBPM').empty();
            // $('#realTimeInfoPanespO2').empty();

            
            // console.log(myChartRedIR.data.datasets[0].data[myChartRedIR.data.datasets[0].data.length - 1]);
            $('#realTimeInfo .toast-body').html($('<div></div>').append("Device: " + 
                device_uuid.slice(0, 5) + '...' + device_uuid.slice(-5)
            ));
            $('#realTimeInfo .toast-body').append($("<div></div>").append("Timestamp: " + 
                moment(new Date(realTimeData[realTimeData.length-1]["serverTimestamp"]))
                .local()
                .format('HH:mm:ss')
            ));
            $('#realTimeInfo .toast-body').append($("<div></div>").append("BPM real time: " + realTimeData[realTimeData.length-1]["BPM"]));
            $('#realTimeInfo .toast-body').append($("<div></div>").append("spO2 real time: " + realTimeData[realTimeData.length-1]["spO2"]));
        });
    }
    else{
        $('#graphDatasheetBtn').show();
        $('#realTimeSpinner').addClass('fa-info-circle').removeClass('fa-spinner fa-spin');
        $('#infoPane').hide();
        $('#realTimeInfo').toast('hide');
        if (socket){
            socket.off('device has new data');
            socket.emit('unsubscribe');
            socket.disconnect();
        }
    }
});
function updateGraphData(realTimeData){

    // myChartRedIR.data.datasets.pop();
    // myChartRedIR.data.datasets.pop();
    // myChartBPM.data.datasets.pop();
    // myChartspO2.data.datasets.pop();
}
function destroyGraphData(){
    // myChartRedIR.data.datasets.pop();
    // myChartRedIR.data.datasets.pop();
    // myChartBPM.data.datasets.pop();
    // myChartspO2.data.datasets.pop();
    // myChartRedIR.update();
    // myChartBPM.update();
    // myChartspO2.update();
    // myChartRedIR.data.datasets.pop();
}
// $('.removeBtnDevice').hover(function() {
//     $(this).closest('a').addClass('show_delete');
//     $(this).addClass('show_delete');
//     // .css("background-color: hotpink;width: 50px;")
// }, function() {
//     $(this).closest('a').removeClass('show_delete');
//     $(this).removeClass('show_delete');
// }) 
$('#deviceSubmenu').on('mouseenter', '.removeBtnDevice', function () {
    //stuff to do on mouse enter
        $(this).closest('a').addClass('show_delete');
        $(this).addClass('show_delete');
    }
)
$('#deviceSubmenu').on('mouseleave', '.removeBtnDevice', function () {
    //stuff to do on mouse leave
        $(this).closest('a').removeClass('show_delete');
        $(this).removeClass('show_delete'); 
    }
)



$(document).on('click', '.removeBtnDevice', () => {
    button = $(event.target);
    device_uuid = button.closest('a').attr('id').substr(7);
    console.log(device_uuid);
    // console.log('s')
    $.ajax({
        type: 'POST',
        url: '/removeDevice',
        data: 'device_uuid=' + device_uuid, // serializes the form's elements.
        beforeSend: async function() {
            // button.find('.fa-plus-square').css("display", "none");
            // $(".loaderSpinner").addClass("loaderSpinnerInline");
            await button.find('svg').removeClass('fa-minus-circle').addClass('fa-spinner').addClass('fa-spin')
        },
        success: data => {
            //- $(".loaderDiv").hide();
            //console.log(response)
            // console.log(data);
            console.log(data);
            alert(data.msg);
            if (data.redirect) 
                window.location.href = data.redirect;

            populateDeviceSubmenuSidebar(data.updated_device_list);
            // $(".loaderSpinner").removeClass("loaderSpinnerInline");
            // form.find('.fa-plus-square').css("display", "inline-block");
            //- new Promise(() => );
            
        },
        error: data => {
            // $(".loaderSpinner").removeClass("loaderSpinnerInline");
            // form.find('.fa-plus-square').css("display", "inline-block");
            alert(data.responseJSON.msg);
        }
    });
})

$("#getDeviceDatasheetForm input[type='radio']").click(()=>{
    if (event.target.id == 'radioGetBytimerangeCheck'){
        $('#getTimerangeInput').addClass('show');
        $('#getByLengthInput').removeClass('show');
    }
    else if (event.target.id == 'radioGetByLengthCheck'){
        $('#getTimerangeInput').removeClass('show');
        $('#getByLengthInput').addClass('show');
    }
    else{
        $('#getTimerangeInput').removeClass('show');
        $('#getByLengthInput').removeClass('show'); 
    }
})

function populateDataTable(device_data=null){
    // if (!device_data.hasOwnProperty('return_data'))
    //     throw('Not valid device data');
    if (!device_data) return;
    here = $('#dataSheetTable');
    if (!$('#dataSheetTable #dataTable' + active_device).length){
        here.append($("<div>", {"class": "dataTable", "id": "dataTable" + active_device}));
    }
    here = $('#dataSheetTable #dataTable' + active_device);
    here.on("activeDeviceChange", function () {
        if ($(this).attr("id") != 'dataTable' + active_device)
            $(this).hide();
        else $(this).show();
    })
    here.empty();
    table_header = $('<thead class="thead-dark"> </thead>').append(
                        $('<tr></tr>').append(
                            $('<th scope="col">').text("#"),
                            $('<th scope="col">').text("Red"),
                            $('<th scope="col">').text("IR"),
                            $('<th scope="col">').text("BPM"),
                            $('<th scope="col">').text("SPO2"),
                            // $('<th scope="col">').text("CPU TIMESTAMP[1]"),
                            // $('<th scope="col">').text("REFERENCE TIME[2]"),
                            $('<th scope="col">').text("MEASUREMENT TIMESTAMP"),
        )
    )
    table_body = $('<tbody></tbody>');
    table = $('<table class="table table-striped table-bordered table-hover"></table>').append(
        table_header,
        table_body
    )
    here.append(table);
    for (i = 0; i < device_data.length; i++){
        // friendlyOriginTimestamp = moment(Date.parse(device_data[i].originTimestamp)).format('DD-MM-YYYY HH:mm:ss.SSS');
        // friendlyMeasumentTimestamp = moment(Date.parse(device_data[i].originTimestamp) + device_data[i].cpuTimestamp).format('DD-MM-YYYY HH:mm:ss.SSS');
        friendlyMeasumentTimestamp = moment(Date.parse(device_data[i].serverTimestamp)).format('DD-MM-YYYY HH:mm:ss.SSS');
        row = $('<tr></tr>').append(
            $('<th scope="row">').text(i),
            $('<td>').text(device_data[i].Red),
            $('<td>').text(device_data[i].IR),
            $('<td>').text(device_data[i].BPM),
            $('<td>').text(device_data[i].spO2),
            // $('<td>').text(device_data[i].cpuTimestamp),
            // $('<td>').text(friendlyOriginTimestamp),
            $('<td>').text(friendlyMeasumentTimestamp),
        )
        table_body.append(row);
    }

}
function collectFormDataBiometricQuery(){
    var form_object = {};
    // var form_data = "device_uuid=" + active_device;
    form_object["device_uuid"] = active_device;
    var type_query = $('input[name=type_query]:checked', '#getDeviceDatasheetForm').val();
    // console.log(type_query);
    if (type_query == "get_by_timerange"){
        timerangeInputCheckList = $('#getTimerangeInput input[type="checkbox"]:checkbox:checked');
        // console.log(timerangeInputCheckList);
        timerangeInputCheckList.each(function(){
            // console.log($(this).val());
            // console.log('#getTimerangeInput input[type="datetime-local"][name="'+$(this).val()+'"]');
            datestring = $('#getTimerangeInput input[type="datetime-local"][name="'+$(this).val()+'"]').val();
            isodate = new Date(datestring).toISOString();
            // form_data += '&' + $(this).val() + '=' + isodate;
            form_object[$(this).val()] = isodate;
        })
    }
    else if (type_query == "get_by_length"){
        getlengthInputCheckList = $('#getByLengthInput input[type="checkbox"]:checkbox:checked');
        // console.log(timerangeInputCheckList);
        getlengthInputCheckList.each(function(){
            // console.log($(this).val());
            // console.log('#getTimerangeInput input[type="datetime-local"][name="'+$(this).val()+'"]');
            num_val = $('#getByLengthInput input[type="number"][name="'+$(this).val()+'"]').val();
            //isodate = new Date(datestring).toISOString();
            // form_data += '&' + $(this).val() + '=' + num_val;
            form_object[$(this).val()] = num_val;
        })
    }
    else{
        // form_data += '&' + type_query + "=1";
        form_object[type_query] = 1;
    }
    return form_object;
}
function serializeJsonObj(obj) {
    return Object
        .keys(obj)
        .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
        .join('&');
}

$('#downloadDatasheetBtn').on('click', () => {
    var url = '/downloadData';
    var request_method = 'POST';
    //var form_data = form.serialize();
    var form_obj = collectFormDataBiometricQuery();
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.name = "HiddenForm";
    form.method = request_method;
    form.action = url;
    for (key in form_obj){
        $('<input>').attr({
            type: 'hidden',
            name: key,
            value: form_obj[key]
        }).appendTo($("form[name='HiddenForm']"));
    }
    $("form[name='HiddenForm']").submit()//.remove();
})

$('#getDeviceDatasheetForm').submit(e => {
    e.preventDefault();
    var form = $(e.target);
    var url = form.attr('action');
    var request_method = form.attr("method");
    //var form_data = form.serialize();
    var form_data = serializeJsonObj(collectFormDataBiometricQuery());
    $.ajax({
        type: request_method,
        url: url,
        data: form_data, // serializes the form's elements.
        beforeSend: function() {
            //form.find('.fa-plus-square').css("display", "none");
            //$(".loaderSpinner").addClass("loaderSpinnerInline");
            // console.log($('#deviceTabHeader a[aria-controls="tab-02"] svg'))
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').removeClass('fa-table');
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').addClass('fa-spinner fa-spin');
        },
        success: data => {
            //- $(".loaderDiv").hide();
            //console.log(response)
            // console.log(data);
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').addClass('fa-table');
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').removeClass('fa-spinner fa-spin');
            alert(data.msg);
            console.log(data);
            if (data.redirect) 
                window.location.href = data.redirect;
            device_sensor_data[data.query_info.Device_uuid] = data.return_data;
            populateDataTable(data.return_data);
            // populateDeviceGraph(data);
            //populateDeviceSubmenuSidebar(data.updated_device_list);
            //$(".loaderSpinner").removeClass("loaderSpinnerInline");
            //form.find('.fa-plus-square').css("display", "inline-block");
            //- new Promise(() => );
            
        },
        error: data => {
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').addClass('fa-table');
            $('#deviceTabHeader a[aria-controls="tab-02"] svg').removeClass('fa-spinner fa-spin');
            // $(".loaderSpinner").removeClass("loaderSpinnerInline");
            // form.find('.fa-plus-square').css("display", "inline-block");
            alert(data.responseJSON.msg);
        }
    });
})

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
$('#graphDatasheetBtn').click(() => {
    // if (isEmpty(device_sensor_data)) return;
    populateDeviceGraph(device_sensor_data[active_device]);
})

$('#reloadDeviceInfoBtn').click(() => {

})

$(document).ready(() => {
    // dummy_data = {return_data:[]};
    var ctxRedRaw = $('#myChartRedRaw');
    window.myChartRedRaw = new Chart(ctxRedRaw, chartOptRedRaw);
    var ctxIRRaw = $('#myChartIRRaw');
    window.myChartIRRaw = new Chart(ctxIRRaw, chartOptIRRaw);
    var ctxBPM =  $('#myChartBPM');
    window.myChartBPM = new Chart(ctxBPM, chartOptBPM);
    var ctxspO2 = $('#myChartspO2');
    window.myChartspO2 = new Chart(ctxspO2, chartOptspO2);
    var ctxRedPulse = $('#myChartRedPulse');
    window.myChartRedPulse = new Chart(ctxRedPulse, chartOptRedPulse);
    var ctxIRPulse = $('#myChartIRPulse');
    window.myChartIRPulse = new Chart(ctxIRPulse, chartOptIRPulse);

    window.redDataset = {
        label: 'Red',
        data: [],
        borderColor: "#FF0000",
        fill: false,
        // draggable: true
    };
    window.redMADataset = {
        label: 'Red Moving Average',
        data: [],
        borderColor: "#00FFFF",
        fill: false,
        // draggable: true
    };
    window.irDataset = {
        label: 'IR',
        data: [],
        borderColor: "#0000FF",
        fill: false
    };
    window.irMADataset = {
        label: 'IR Moving Average',
        data: [],
        borderColor: "#FF8C00",
        fill: false
    };
    window.spO2Dataset = {
        label: 'spO2',
        data: [],
        borderColor: "#9400D3",
        fill: false
    };
    window.bpmDataset = {
        label: 'BPM',
        data: [],
        borderColor: "#FFD700",
        fill: false
    };

    window.redPulseDataset = {
        label: 'Red Pulse',
        data: [],
        borderColor: "#FF0000",
        fill: false
    };
    window.irPulseDataset = {
        label: 'IR Pulse',
        data: [],
        borderColor: "#0000FF",
        fill: false
    };

    window.myChartRedRaw.data.datasets.push(window.redDataset);
    window.myChartRedRaw.data.datasets.push(window.redMADataset);
    window.myChartIRRaw.data.datasets.push(window.irDataset);
    window.myChartIRRaw.data.datasets.push(window.irMADataset);
    window.myChartBPM.data.datasets.push(window.bpmDataset);
    window.myChartspO2.data.datasets.push(window.spO2Dataset);
    window.myChartRedPulse.data.datasets.push(window.redPulseDataset);
    // myChartRedPulse.data.datasets.push(detectPulseDataset);
    window.myChartIRPulse.data.datasets.push(window.irPulseDataset);
    // populateDeviceGraph(dummy_data);

    myChartspO2.options.scales.yAxes[0].ticks.max=100;
    myChartspO2.options.scales.yAxes[0].ticks.min=0;
    myChartBPM.options.scales.yAxes[0].ticks.max=180;
    myChartBPM.options.scales.yAxes[0].ticks.min=0;
    myChartRedPulse.options.scales.yAxes[0].ticks.max=2000;
    myChartRedPulse.options.scales.yAxes[0].ticks.min=-2000;
    myChartIRPulse.options.scales.yAxes[0].ticks.max=2000;
    myChartIRPulse.options.scales.yAxes[0].ticks.min=-2000;

    myChartRedRaw.update();
    myChartIRRaw.update();
    myChartBPM.update();
    myChartspO2.update();
    myChartRedPulse.update();
    myChartIRPulse.update();

    window.mapStringToChartObject = {
        myChartRedRaw: window.myChartRedRaw,
        myChartIRRaw: window.myChartIRRaw,
        myChartBPM: window.myChartBPM,
        myChartspO2: window.myChartspO2,
        myChartRedPulse: window.myChartRedPulse,
        myChartIRPulse: window.myChartIRPulse,
    }
    $('#realTimeInfo2').toast('show');
})


// $('.removeBtnDevice').click(() => {
//     console.log('Hello')
// })
//- $(".navbar-nav .nav-item.dropdown").hover(
//-     function(){
//-         $(this).children('.dropdown-menu').addClass('show');
//-     }, 
//-     function(){
//-         $(this).children('.dropdown-menu').removeClass('show');
//-     }
//- )
//- ("#addDeviceUuidDropdown").