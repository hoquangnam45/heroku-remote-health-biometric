const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

//- console.log($(location).attr('hash'));
//- console.log($(location).attr('href'));

signUpButton.addEventListener('click', () => {
    window.location.hash = "sign-up";
    container.classList.add("right-panel-active");
    //- if($(".container").hasClass("forgot-password-active")){
        $(".container").removeClass("forgot-password-active");
    //- }
    
});

signInButton.addEventListener('click', () => {
    window.location.hash = "sign-in";
    container.classList.remove("right-panel-active");
    //- if($(".container").hasClass("forgot-password-active")){
        $(".container").removeClass("forgot-password-active");
    //- }
    
});

$(".overlay-panel a").click(() => {
    //$(".container").toggleClass("forgot-password-active");
    //- if(!$(".container").hasClass("forgot-password-active"))
        $(".container").addClass("forgot-password-active");
    if($(".container").hasClass("right-panel-active"))
        $(".form-container.forgot-password-container .fa-arrow-right").removeClass("fa-arrow-right").addClass("fa-arrow-left");
    else
        $(".form-container.forgot-password-container .fa-arrow-left").removeClass("fa-arrow-left").addClass("fa-arrow-right");
})

$(".form-container.forgot-password-container span").click(() => {
    $(".container").removeClass("forgot-password-active");
})

$("#sign-in-form, #sign-up-form, #forgot-password-form").submit(e => {
    //- console.log('test');
    e.preventDefault();
    var form = $(e.target);
    var url = form.attr('action');
    var request_method = form.attr("method");
    var form_data = form.serialize(); 
    //var test = $("#sign-in-form").attr("action")
    //- $(".loaderSpinner").addClass("loaderSpinnerInline");
    $.ajax({
        type: request_method,
        url: url,
        data: form_data, // serializes the form's elements.
        beforeSend: function() {
            $(".loaderSpinner").addClass("loaderSpinnerInline");
        },
        success: data => {
            //- $(".loaderDiv").hide();
            //console.log(response)
            alert(data.msg);
            if (data.redirect) 
                window.location.href = data.redirect;
            $(".loaderSpinner").removeClass("loaderSpinnerInline");
            //- new Promise(() => );
            
        },
        error: data => {
            $(".loaderSpinner").removeClass("loaderSpinnerInline");
            alert(data.responseJSON.msg);
        }
    });
});

var url_hash = $(location).attr('hash');
if (!url_hash){
    window.location.hash = "sign-in";
}
if (url_hash == "#sign-in"){
    $(".container").addClass("right-panel-active");
    $(".overlay-container, .form-container").addClass('disable-transition-time');
}

$( document ).ready(() => {
    if (url_hash){  
        if (url_hash == "#sign-in"){
            //$(".container").addClass("right-panel-active");
            $(".overlay-container, .form-container").removeClass('disable-transition-time');
            $(".container").removeClass("right-panel-active");
        }
        else if (url_hash == "#sign-up"){
            $(".container").addClass("right-panel-active");
            //$(".container").removeClass("forgot-password-active");
        }
        else if (url_hash == "#forgot-password"){
            $(".container").addClass("forgot-password-active");
            if($(".container").hasClass("right-panel-active"))
                $(".form-container.forgot-password-container .fa-arrow-right").removeClass("fa-arrow-right").addClass("fa-arrow-left");
            else
                $(".form-container.forgot-password-container .fa-arrow-left").removeClass("fa-arrow-left").addClass("fa-arrow-right");
        }
        //- $('#sign-in:target, #sign-up:target, #forgot-password-a, #forgot-password-b')
    }
})