require('dotenv').config()
const express = require('express');
const session = require('express-session');
var passport = require('./controller/authenticateUser');
//var LocalStrategy = require('passport-local').Strategy;

const redisStore = require('connect-redis')(session);
const {cacher} = require('./controller/cacher');
const {CACHER_PREFIX} = require('./controller/cacher');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io').listen(http);
//var uuid = require('uuid');
var path = require('path');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var flash = require('express-flash');
var cors = require('cors');
const trimRequest = require('trim-request');

var httplogger = require('morgan');
var logger = require('./controller/logger');
//require('./controller/mongodb_driver').connectDatabase();

var mongodb_driver = require('./controller/mongodb_driver');
mongodb_driver.connectDatabase();

var realtime_event_driver = require('./controller/handle_event')(io, mongodb_driver);

var deviceRouter = require('./routes/device_route');
var indexRouter = require('./routes/index_route');
var userRouter = require('./routes/user_route');
// const ip = require('ip');
// const promise = require('promise');

//console.log(uuid.v4()); // test uuid: 3c8f06ad-75df-4734-84d8-898938eb4aa4

// view engine setup
app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'pug'); // Use jade(pug) template engine

app.use(httplogger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 

app.use(cookieParser());
app.use(session({ 
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: false,
    store: new redisStore({client: cacher, ttl: 86400}),
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(cors());
app.set('json spaces', 4)

app.use(trimRequest.all);

// Set route
app.use('/', indexRouter); // Interact with database
app.use('/device', deviceRouter); // Interact with device database
// app.use('/subscribe') // Get real time data
app.use('/user', userRouter); // Interact with user database

//app.use('/user',)


// Spin up server
const PORT = process.env.PORT || 3000;
http.listen(PORT, () => {
    logger.info('listening on: ' + PORT);
});

// console.log(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test('3c8f06ad-75df-4734-84d8-898938eb4aa4'));
// logger.info(Date.now());

//module.exports = app;
// function isTrue(param){
//     if (param == null || param == 0 || param == 'false') return false;
//     return true;
// }

// const asyncRedis = require("async-redis");
// const myCache = asyncRedis.createClient();

// myCache.on("error", function(err) {
//     console.log("Error " + err);
// });

// const asyncBlock = async() => {
//     await myCache.set("k1", "val1");
//     // console.log(1);
//     const value = await myCache.get("k1");
//     // console.log(2);
//     console.log("val2",value);

// };
// var setValue = async(key, value) => {
//     let ret = await myCache.set(key, value);
//     return ret;
//     // console.log(ret);
// };

// var getValue = async(key) => {
//     let val = await myCache.get(key);
//     return val;
//     // console.log(val);
// };
// (async() => {
// asyncBlock(); //  working 
// console.log(await setValue("aa", "bb")); // but this isn't 
// console.log("val", getValue("aa"));//but this isn't 
// console.log("vasl", getValue("aa"));//but this isn't 
// })();