const nodemailer = require('nodemailer');
var logger = require('./logger');

let client = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'hoquangnam69@gmail.com',
        pass: 'sggfocwrcurwrqup'
    }
});

function checkEmail(email){
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function sendResetEmail(toEmailAddress, fromHost, token){
    var email = {
        from: 'Remote health biometric <hoquangnam69@gmail.com>',
        to: toEmailAddress,
        subject: 'Password Reset',
        
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                'http://' + fromHost + '/reset_password/' + token + '\n\n' +
                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    }
    client.sendMail(email).catch(err => logger.info(err));
}

function sendConfirmationReset(to_user){
    var email = {
        from: 'Remote health biometric <hoquangnam69@gmail.com>',
        to: to_user.email,
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + to_user.email + ' has just been changed.\n'
    };
    client.sendMail(email).catch(err => logger.info(err));
}

module.exports = {
    sendResetEmail,
    sendConfirmationReset,
    checkEmail
}