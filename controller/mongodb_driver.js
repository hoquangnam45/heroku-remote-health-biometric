const mongoose = require("mongoose");
var logger = require ('./logger');
var sensordb_driver = null;
var userdb_driver = null;

const DEFAULT_MONGOOSE_OPTIONS = {
    keepAlive: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: true
  };
  
const DEFAULT_MONGODB_URISTRING =
    process.env.MONGODB_URI ||
    process.env.MONGODB_URL;

function connectDatabase(mongodb_uristring = DEFAULT_MONGODB_URISTRING, mongoose_options = DEFAULT_MONGOOSE_OPTIONS){
    mongoose.pluralize(null);
    mongoose.connect(mongodb_uristring, mongoose_options, function (err, res) {
        if (err) {
            throw("ERROR connecting to mongodb: " + mongodb_uristring + ". " + err);
        } else {
            logger.info("Succeeded connected to mongodb: " + mongodb_uristring);
        }
    });

    sensordb_driver = require('./sensordb_driver');
    userdb_driver = require('./userdb_driver');
    sensordb_driver.initModel();
    userdb_driver.initModel();
    module.exports.sensordb_driver = sensordb_driver;
    module.exports.userdb_driver = userdb_driver;
}

function checkMongooseConnectionState(){
    switch(mongoose.connection.readyState){
        case 0: // disconnected
            throw("Mongoose not connect to database, pls connect to database first");
            //break;
        case 1: // connected
            return;
            //break;
        case 2: // connecting
            throw("Mongoose is connecting, pls wait");
            //break;
        case 3: // disconnecting
            throw("Mongoose not connect to database, pls connect to database first");
            //break;
        default:
            break;
    }
}

function disconnectDatabase(){
    mongoose.connection.close();
}

module.exports = {
    connectDatabase,
    checkMongooseConnectionState,
    disconnectDatabase,
    sensordb_driver,
    userdb_driver
}