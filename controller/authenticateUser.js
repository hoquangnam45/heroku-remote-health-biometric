var session = require('express-session')
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var async = require('async');
var crypto = require('crypto');
var schema = require('./schema');
const logger = require('./logger');
const {cacher} = require('./cacher');
const {CACHER_PREFIX} = require('./cacher');
var email_checker = require('./send_mail').checkEmail;
var mongodb_driver = require('./mongodb_driver');

const EXPIRE_CACHE_TIME_SECOND = 3600;

schema.userSchema.pre('save', function(next) {
    var user = this;
    var SALT_FACTOR = 5;
  
    if (!user.isModified('password')) return next();
  
    bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

// schema.userSchema.methods.comparePassword = async function(candidatePassword) {
//     let isMatch = await bcrypt.compare(candidatePassword, this.password);
//     return(isMatch);
// };


async function handleAuthenticate(user, password, done){
    if (!user) return done(null, false, {message: 'Incorrect login info.'});
    let isMatch =  await bcrypt.compare(password, user.password);
    if (isMatch) 
        return done(null, user);
    else{
        throw('Incorrect password');
        //return done(null, false, { message: 'Incorrect password.' });
    }
}

passport.use(
    'login with email',
    new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, 
    async (email, password, done) => {
    try {
        let user = await mongodb_driver.userdb_driver.User.findOne({email : email}).lean();
        await handleAuthenticate(user, password, done);
    }
    catch(err){
        throw(err);
    }
}));

passport.use(
    'login with username',
    new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, 
    async (username, password, done) => {
    try {
        let user = await mongodb_driver.userdb_driver.User.findOne({username: username}).lean();
        await handleAuthenticate(user, password, done);
    }
    catch(err){
        throw(err);
    }
}));

passport.use(
    'login with login info',
    new LocalStrategy({
        usernameField: 'login_info',
        passwordField: 'password'
    }, 
    async (login_info, password, done) => {
    try {
        var user;
        if(email_checker(login_info)) 
            user = await mongodb_driver.userdb_driver.User.findOne({email: login_info}).lean(); 
        else
            user = await mongodb_driver.userdb_driver.User.findOne({username: login_info}).lean();
        await handleAuthenticate(user, password, done);
    }
    catch(err){
        done(err);
    }
}));

passport.serializeUser(async(user, done) => {
    try{
        done(null, user._id); // Add to session
        cacher.setAsync(CACHER_PREFIX.USER + user._id, JSON.stringify(user));
    }
    catch(err){
        done(err, user._id);
        logger.info(err);
    }    
});
  
passport.deserializeUser(async(_id, done) => {
    try{
        let cached_data = await cacher.getAsync(CACHER_PREFIX.USER + _id);
        if (!cached_data){
            let user = await mongodb_driver.userdb_driver.User.findById(_id).lean();
            cacher.setAsync(CACHER_PREFIX.USER + user._id, JSON.stringify(user));
            done(null, user);
        }
        else {
            var user = JSON.parse(cached_data);
            done(null, user); // cache hit
        }
    }
    catch(err){
        done(err, _id);
        logger.info(err);
    }
});

module.exports = passport;
