var {cacher} = require('./cacher');
const {CACHER_PREFIX} = require('./cacher');
const mongoose = require("mongoose");
var logger = require('./logger');
var schema = require('./schema');
var passport = require('./authenticateUser');
var email_checker = require('./send_mail').checkEmail;
var mongodb_driver = require('./mongodb_driver');
var crypto = require('crypto');
var User;

function initModel(){
    User = mongoose.model("remote-biometric-user",  schema.userSchema);
    User.on('index', err => { 
        if (err) logger.info(err);
    });
    module.exports.User = User;
}

function registerUser(userInfo){
    User.createIndexes();
    return new Promise(async(resolve, reject) => {
        try{
            var user = new User(userInfo);
            let dup_check = await User.exists({$or: [
                {username: user.username},
                {email: user.email}
            ]});
            if (dup_check) reject('User already exist');
            let ret = await user.save();
            resolve({
                msg:'Register user successfully', 
                user: ret
            });
        }
        catch(err){
            reject(err);
        }
    })
}

function genResetPasswordToken(login_info){
    return new Promise(async(resolve, reject) => {
        try{
            var user;
            if (!login_info) reject('Empty field');
            let reset_token = crypto.randomBytes(20).toString('hex');
            if(email_checker(login_info)) 
                user = await mongodb_driver.userdb_driver.User.findOneAndUpdate({email: login_info}, {
                    resetPasswordToken : reset_token,
                    resetPasswordExpires : Date.now() + 3600000 // 1 hour
                }, {new: true}).lean(); 
            else
                user = await mongodb_driver.userdb_driver.User.findOneAndUpdate({username: login_info}, {
                    resetPasswordToken : reset_token,
                    resetPasswordExpires : Date.now() + 3600000 // 1 hour
                }, {new: true}).lean(); 
            if (!user) reject('Incorrect login info.');
            
            resolve({
                email: user.email,
                token: user.resetPasswordToken
            })
        }
        catch(err){
            reject(err);
        }
    });
}

function getUserWithResetToken(reset_token){
    return new Promise(async(resolve, reject) => {
        try{
            if (!reset_token) reject('Empty token');
            let user = await mongodb_driver.userdb_driver.User.findOne({
                resetPasswordToken: reset_token,  
                resetPasswordExpires: {$gt: Date.now()}
            });
            if (!user) reject('Password reset token is invalid or has expired.');
            resolve(user);
        }
        catch(err){
            reject(err);
        }
    });
}

/****************************** */
function addDevice(to_user, device_uuid){
    return new Promise(async(resolve, reject) => {
        try{
            if (!device_uuid) reject('Lack device uuid');
            let device = await mongodb_driver.sensordb_driver.Device_info.findOne({Device_uuid: device_uuid});
            if (!device){
                reject('No found device in database');
            }
            
            let user = await User.findOneAndUpdate({username: to_user.username,
                                                    associatedDevice: {
                                                        $ne: device._id
                                                    }},
                                                    {$push: {"associatedDevice": (device._id)}},
                                                    {new: true}).lean();
            // logger.info(device.associatedUser);
            if(!user) reject('Device has already associated with this account');

            device.associatedUser.push(user._id);//.addToSet(user._id);
            await device.save();
            // await mongodb_driver.sensordb_driver.Device_info.findOneAndUpdate({Device_uuid: device_uuid},
            //                                 {$addToSet: {"associatedUser": (user._id)}},
            //                                 {new: true}).lean();
            // logger.info(user);
            cacher.setAsync(CACHER_PREFIX.USER + user._id, JSON.stringify(user)); // Updating user cache after modify device list
            // logger.info('Save device ' + device.Device_uuid + ' to user ' + user.username);
            resolve({
                msg: 'Device ' + device.Device_uuid + ' successfully added',
                updated_user: user
            });
        }
        catch(err){
            reject(err)
        };
    })
}

/****************************** */
function removeDevice(from_user, device_uuid){
    return new Promise(async(resolve, reject) => {
        try{
            if (!device_uuid) reject('Lack device uuid');
            let device = await mongodb_driver.sensordb_driver.Device_info.findOne({Device_uuid: device_uuid});
            if (!device){
                reject('No found device in database');
            }
            let user = await User.findOneAndUpdate({username: from_user.username, 
                                                    associatedDevice: {
                                                        $eq: device._id
                                                    }},
                                                    {$pull: {"associatedDevice": device._id}},
                                                    {new: true}).lean();
            
            if(!user) reject('No device with this uuid associated with this account');
            // logger.info(device.associatedUser);
            device.associatedUser.pull(user._id);
            await device.save();


                
            // user.associatedDevice.pull(device._id); 
            // device.associatedUser.pull(user._id);
            // await Promise.all([user.save(), device.save()]);
            // await mongodb_driver.sensordb_driver.Device_info.findOneAndUpdate({Device_uuid: device_uuid},
            //                                 {$pull: {"associatedUser": ()}},
            //                                 {new: true}).lean();
            // logger.info(user);
            cacher.setAsync(CACHER_PREFIX.USER + user._id, JSON.stringify(user)); // Updating user cache after modify device list            logger.info('Remove device ' + ret.Device_uuid + ' from user ' + user.username);
            resolve({
                msg: 'Device ' + device.Device_uuid + ' successfully removed',
                updated_user: user
                // updated_device: 
            });
        }
        catch(err){
            reject(err)
        };
    })
}

function getDeviceInfo(device_id_list){
    return new Promise(async (resolve, reject) => {
        try{
            var device_obj = [];
            var promise_arr = [];
            for (var i = 0; i < device_id_list.length; i++){
                let promise = cacher.getAsync(CACHER_PREFIX.DEVICE + device_id_list[i]);
                promise_arr.push(promise);
            }
            let cached_data = await Promise.allSettled(promise_arr);
            for (var i = 0; i < cached_data.length; i++){
                if(cached_data[i].value){
                    promise_arr[i] = JSON.parse(cached_data[i].value);
                    continue;
                }
                promise_arr[i] = await mongodb_driver.sensordb_driver.Device_info.findById(device_id_list[i]).lean();
            }
            let device_data = await Promise.allSettled(promise_arr);
            for (var i = 0; i < device_data.length; i++){
                if(!device_data[i].value)
                    reject('Device uuid is missing from db');
                device_obj.push({
                    // id: device_data[i].value._id,
                    _id: device_data[i].value._id,
                    Device_uuid: device_data[i].value.Device_uuid,
                    Device_name: device_data[i].value.Device_name
                })
                // logger.info(device_data[i])
                // logger.info('/.')
                // logger.info(device_obj[i])
                // logger.info(device_data[i].value)
                cacher.setAsync(CACHER_PREFIX.DEVICE + device_obj[i].id, JSON.stringify(device_obj[i]));
            }
            resolve(device_obj);
        }
        catch(err) {
            reject(err);
        }
    });
}

function getDeviceInfoByUuid(device_uuid_list){
    return new Promise(async (resolve, reject) => {
        try{
            var device_obj = [];
            var promise_arr = [];
            // for (var i = 0; i < device_id_list.length; i++){
            //     let promise = cacher.getAsync(CACHER_PREFIX.DEVICE + device_id_list[i]);
            //     promise_arr.push(promise);
            // }
            // let cached_data = await Promise.allSettled(promise_arr);
            for (var i = 0; i < device_uuid_list.length; i++){
                // if(cached_data[i].value){
                //     promise_arr[i] = JSON.parse(cached_data[i].value);
                //     continue;
                // }
                promise_arr[i] = await mongodb_driver.sensordb_driver.Device_info.findById(device_id_list[i]).lean();
            }
            let device_data = await Promise.allSettled(promise_arr);
            for (var i = 0; i < device_data.length; i++){
                if(!device_data[i].value)
                    reject('Device uuid is missing from db');
                device_obj.push({
                    // id: device_data[i].value._id,
                    _id: device_data[i].value._id,
                    Device_uuid: device_data[i].value.Device_uuid,
                    Device_name: device_data[i].value.Device_name
                })
                // logger.info(device_data[i])
                // logger.info('/.')
                // logger.info(device_obj[i])
                // logger.info(device_data[i].value)
                cacher.setAsync(CACHER_PREFIX.DEVICE + device_obj[i].id, JSON.stringify(device_obj[i]));
            }
            resolve(device_obj);
        }
        catch(err) {
            reject(err);
        }
    });
}

function flushUserCache(user_id){
    cacher.delAsync(CACHER_PREFIX.USER + user_id);
}
module.exports = {
    initModel,
    registerUser,
    addDevice,
    removeDevice,
    genResetPasswordToken,
    getUserWithResetToken,
    getDeviceInfo,
    flushUserCache,
    User
}