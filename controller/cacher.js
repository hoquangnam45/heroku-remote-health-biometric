var promise = require('bluebird');
const redis = promise.promisifyAll(require('redis'));
const logger = require('./logger');
const DEFAULT_CACHER_URISTRING =
    process.env.REDIS_URL;

    const cacher = redis.createClient(DEFAULT_CACHER_URISTRING);

cacher.on('error', (err) => {
    logger.info('Cacher error: ', err);
});

const USER = "db_user_";
const DEVICE = "db_device_";

module.exports = {
    cacher,
    CACHER_PREFIX: {
        USER,
        DEVICE
    }
}
