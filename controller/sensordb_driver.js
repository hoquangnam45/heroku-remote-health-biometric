var {cacher} = require('./cacher');
const {CACHER_PREFIX} = require('./cacher');
const mongoose = require("mongoose");
var logger = require('./logger');
var schema = require('./schema');
var mongodb_driver = require('./mongodb_driver');
const { query } = require('express');
var Biometric_data;
var Device_info;

const DEFAULT_PAGE_LIMIT = 100;
const DEFAULT_PAGE_OFFSET = 0;

function isIsoDate(str) {
    if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(str)) return false;
    var d = new Date(str); 
    return d.toISOString()===str;
}

//
function initModel(){
    Biometric_data = mongoose.model("remote-biometric-data",  schema.sensorRecordSchema);
    Biometric_data.on('index', err => { 
        if (err) logger.info(err);
    })
    Device_info = mongoose.model("remote-biometric-device",  schema.deviceInfoSchema);
    Device_info.on('index', err => { 
        if (err) logger.info(err);
    })
    module.exports.Biometric_data = Biometric_data;
    module.exports.Device_info = Device_info;
}

//
function createNewDevice(device_uuid, device_name){
    return new Promise(async (resolve, reject) => {
        try{
            if (device_uuid) 
                var insert_uuid = device_uuid;
            else reject('Error: Registering device without device uuid');
            if (await Device_info.exists({Device_uuid: device_uuid}))
                reject('Create duplicate device');
            if (device_name)
                var insert_name = device_name;
            else insert_name = 'Unnamed sensor ' + insert_uuid;
            var new_device = new Device_info({
                Device_uuid: insert_uuid,
                Device_name: insert_name
            });
            
            await new_device.save();
            resolve('Mongodb: Create device ' + device_name + ' with uuid ' + device_uuid + ' successfully');
        }
        catch(err){
            reject(err);
        }
    });
}

//
function setDeviceName(device_uuid, device_name){
    return new Promise(async (resolve, reject) => {
        try{
            if (!device_uuid) reject('Error: Cannot rename without device uuid');
            if (!device_name) reject('Error: No name field parameter request');
            let device = await Device_info.findOneAndUpdate({Device_uuid: device_uuid}, {Device_name: device_name}, {new: true}).lean();
            if (!device) reject('No device with uuid ' + device_uuid + ' to set device name');
            await cacher.delAsync(CACHER_PREFIX.DEVICE + device._id); // Remove cache after modify
            resolve('Mongodb: Update device with uuid ' + device_uuid + ' with new name \'' + device_name + '\' successfully');
        }
        catch(err){
            reject(err);
        }
    });
}

function updateDeviceSensorData(insertRecord, dry_run=false){
    return new Promise(async (resolve, reject) => {
        try{
            if (!insertRecord.device_uuid) 
                reject('Error: No uuid');
            let device = await Device_info.findOne({Device_uuid: insertRecord.device_uuid}).lean();
            if(!device) reject('No device with uuid ' + device_uuid + ' to update device sensor');
            
            insertRecordRef = insertRecord.payload.map(singleRecord => {
                singleRecordRef = Object.assign({}, singleRecord);
                singleRecordRef["deviceInfo"] = device._id;
                return singleRecordRef;
            })
            
            if (dry_run) return resolve({msg: 'Mongodb: This is a dry-run , no data insert to db ' + insertRecord.device_uuid, insertedData: insertRecord.payload});
            savedDoc = await Biometric_data.insertMany(insertRecordRef); 
            // resolve('Mongodb: Successfully insert to device ' + device_uuid);
            return resolve({msg: 'Mongodb: Successfully insert to device ' + insertRecord.device_uuid, insertedData: insertRecordRef});
        }
        catch(err){
            reject(err);
        }
    })
}

function dropDeviceSensorData(device_uuid){
    return new Promise(async (resolve, reject) => {
        try{
            if (!device_uuid) reject('Error: Cannot drop sensor data without device uuid');
            let device = await Device_info.findOne({Device_uuid: device_uuid}).lean();
            if(!device) reject('No device with uuid ' + device_uuid + ' to drop device sensor data');
            await Biometric_data.deleteMany({deviceInfo: device._id}).lean();
            resolve('Mongodb: Drop sensor data of device with uuid ' + device_uuid + ' successfully');
        }
        catch(err){
            reject(err);
        }
    });
}

function dropDevice(device_uuid){
    return new Promise(async (resolve, reject) => {
        try{
            if (!device_uuid) reject('No uuuid to drop device');
            // logger.info(typeof device_uuid);
            //reject('hh');
            let deleted_device = await Device_info.findOneAndDelete({Device_uuid: device_uuid}).lean();
            if (!deleted_device) reject('No device with uuid ' + device_uuid + ' to drop device');
            let delete_device_data = await Biometric_data.deleteMany({deviceInfo: deleted_device._id}).lean();
            await mongodb_driver.userdb_driver.User.findOneAndUpdate({_id: {$in: deleted_device.associatedUser}}, {$pull: {"associatedDevice": deleted_device._id}}).lean();
            resolve('Ddrop and delete device data with uuid ' + device_uuid + ': ' + delete_device_data.deletedCount + ' out of ' + delete_device_data.n + ' matched result');
        }
        catch(err){
            reject(err);
        }
    })
}

function queryDatabase(queryInfo){
    return new Promise(async (resolve, reject) => {
        try{
            resolve(queryDatabaseStream(queryInfo));
        }
        catch(err){
            reject(err);
        }
    })
}

function queryDatabaseStream(queryInfo){
    return new Promise(async (resolve, reject) => {
        try{
            if (!queryInfo.Device_uuid) reject('Cannot get data without device uuid');
            let device = await Device_info.findOne({Device_uuid: queryInfo.Device_uuid}).lean();
            if (!device) reject('No device with uuid ' + queryInfo.Device_uuid + ' to query');
            
            var page_limit;
            var page_offset;
            
            if (!queryInfo.noOffsetLimit){
                if (isNaN(Number(queryInfo.limit)) || Number(queryInfo.limit) < 0)
                    page_limit = DEFAULT_PAGE_LIMIT;
                else page_limit = Math.floor(Number(queryInfo.limit));
                if (isNaN(Number(queryInfo.offset)) || Number(queryInfo.offset) < 0)
                    page_offset = DEFAULT_PAGE_OFFSET;
                else page_offset = Math.floor(Number(queryInfo.offset));
            }
            else{
                page_limit = null;
                page_offset = null;
            }
            
            var return_data = [];
            var used_query_info = {};
            var count_doc = 0;
            var get_data;
            
            used_query_info.Device_uuid = queryInfo.Device_uuid;

            if (queryInfo.count_data == 'true' || Number(queryInfo.count_data) == 1){
                get_data = false;
                used_query_info.count_data = true;
            }
            else {
                get_data = true;
                used_query_info.count_data = false;
            }

            var type_query;
            if (queryInfo.get_latest){
                if (Number(queryInfo.get_latest) != 1 && queryInfo.get_latest != 'true')
                    reject('Check query value, bad value query latest');
                type_query = 'get_latest';
            } // get latest | valid : get_latest = true or 1
            else if (queryInfo.get_length  || queryInfo.index_start || queryInfo.index_end){
                if (isNaN(Number(queryInfo.get_length)) && isNaN(Number(queryInfo.index_start)) && isNaN(Number(queryInfo.index_end)))
                    reject('Check query value, bad value query by length');
                type_query = 'get_by_length';
            } // get by length
            else if (queryInfo.timerange_start || queryInfo.timerange_end){
                if (!isIsoDate(queryInfo.timerange_end) && !isIsoDate(queryInfo.timerange_start))
                    reject('Check time range value, must be ISO8061 format');
                type_query = 'get_by_timerange';
            } // get by timerange | valid : one of timerange_start or timerange_end must be in ISO format 
            else if (queryInfo.get_all){
                if (Number(queryInfo.get_all) != 1 && queryInfo.get_all != 'true')
                    reject('Check query value, bad value query all');
                type_query = 'get_all'
            } // get all 
            else type_query = 'get_none'
            
            used_query_info.type_query = type_query;

            switch(type_query){
                case 'get_all':
                    if (!get_data) {
                        count_doc = await Biometric_data.countDocuments({deviceInfo: device._id});
                        break;
                    }
                    return_data = Biometric_data.find({deviceInfo: device._id})
                                                .sort({serverTimestamp: 1})
                                                .skip(page_offset)
                                                .limit(page_limit)
                                                .lean()
                                                .cursor();
                    break;
                case 'get_by_timerange':
                    timerange_start = Date.parse(queryInfo.timerange_start);
                    timerange_end = Date.parse(queryInfo.timerange_end);
                    if (!isNaN(timerange_start) && !isNaN(timerange_end)){
                        if (timerange_end < timerange_start) 
                            reject('Error end timerange is before start timerange');

                        used_query_info.timerange_start = queryInfo.timerange_start;
                        used_query_info.timerange_end = queryInfo.timerange_end;

                        
                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({
                                deviceInfo: device._id, 
                                serverTimestamp: {$gt: timerange_start, $lt: timerange_end}
                            })
                            break;
                        }
                        return_data = Biometric_data.find({
                                        deviceInfo: device._id, 
                                        serverTimestamp: {$gt: timerange_start, $lt: timerange_end}
                                        })
                                        .sort({serverTimestamp: 1})
                                        // .skip(page_offset)
                                        // .limit(page_limit)
                                        .lean()
                                        .cursor();
                    }
                    else if (!isNaN(timerange_start) && isNaN(timerange_end)){

                        used_query_info.timerange_start = queryInfo.timerange_start;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({
                                deviceInfo: device._id, 
                                serverTimestamp: {$gt: timerange_start}
                            })
                            break;
                        }
                        return_data = Biometric_data.find({
                                        deviceInfo: device._id, 
                                        serverTimestamp: {$gt: timerange_start}
                                        })
                                        .sort({serverTimestamp: 1})
                                        // .skip(page_offset)
                                        // .limit(page_limit)
                                        .lean()
                                        .cursor();
                    }
                    else {
                        
                        used_query_info.timerange_end = queryInfo.timerange_end;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({
                                deviceInfo: device._id, 
                                serverTimestamp: {$lt: timerange_end}
                            })
                            break;
                        }
                        return_data = Biometric_data.find({
                                        deviceInfo: device._id, 
                                        serverTimestamp: {$lt: timerange_end}
                                        })
                                        .sort({serverTimestamp: 1})
                                        // .skip(page_offset)
                                        // .limit(page_limit)
                                        .lean()
                                        .cursor();
                    }     
                    break;
                case 'get_by_length':
                    if (Number(queryInfo.index_start) >= 0 && 
                        Number(queryInfo.index_end) >= 0 && 
                        Number(queryInfo.index_end) > Number(queryInfo.index_start)){
                        
                        used_query_info.index_start = queryInfo.index_start;
                        used_query_info.index_end = queryInfo.index_end;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                            .sort({serverTimestamp: 1})
                            .skip(Math.floor(Number(queryInfo.index_start)))
                            .limit(Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.index_start)))
                            break;
                        }

                        // if (Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.index_start)) - page_offset < page_limit)
                        //     page_limit = Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.index_start)) - page_offset;
                        
                        return_data = Biometric_data.find({deviceInfo: device._id})
                            // .skip(Math.floor(Number(queryInfo.index_start)) + page_offset)
                            // .limit(page_limit)
                            // .skip(page_offset)
                            // .limit(page_limit)
                            .sort({serverTimestamp: 1})
                            .skip(Math.floor(Number(queryInfo.index_start)))
                            .limit(Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.index_start)))
                            .lean()
                            //.sort({_id: -1})
                            .cursor();
                    }
                    else if (Number(queryInfo.index_start) >= 0 && 
                            Number(queryInfo.get_length) >= 0){

                        used_query_info.index_start = queryInfo.index_start;
                        used_query_info.get_length = queryInfo.get_length;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                            .sort({serverTimestamp: 1})
                            .skip(Math.floor(Number(queryInfo.index_start)))
                            .limit(Math.floor(Number(queryInfo.get_length)));
                            break;
                        }

                        if(Math.floor(Number(queryInfo.get_length)) - page_offset < page_limit)
                            page_limit = Math.floor(Number(queryInfo.get_length)) - page_offset

                        return_data = Biometric_data.find({deviceInfo: device._id})
                            // .skip(Math.floor(Number(queryInfo.index_start)) + page_offset)
                            // .limit(page_limit)
                            .sort({serverTimestamp: 1})
                            .skip(Math.floor(Number(queryInfo.index_start)))
                            .limit(Math.floor(Number(queryInfo.get_length)))
                            .lean()
                            //.sort({_id: -1})
                            .cursor();
                        
                    }
                    else if (Number(queryInfo.index_end) >= 0 && 
                            Number(queryInfo.get_length) >= 0){
                        
                        used_query_info.index_end = queryInfo.index_end;
                        used_query_info.get_length = queryInfo.get_length;
                        
                        if (Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.get_length)) > 0){
                            skip_start = Math.floor(Number(queryInfo.index_end)) - Math.floor(Number(queryInfo.get_length));
                            limit_end = Math.floor(Number(queryInfo.get_length));
                        }
                        else {
                            skip_start = 0;
                            limit_end = Math.floor(Number(queryInfo.index_end));
                        }

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                            .sort({serverTimestamp: 1})
                            .skip(skip_start)
                            .limit(limit_end);
                            break;
                        }

                        if(limit_end - skip_start - page_offset < page_limit)
                            page_limit = limit_end - skip_start - page_offset;
                        return_data = Biometric_data.find({deviceInfo: device._id})
                            // .skip(skip_start + page_offset)
                            // .limit(page_limit)
                            .sort({serverTimestamp: 1})
                            .skip(skip_start)
                            .limit(limit_end)
                            .lean()
                            //.sort({_id: -1})
                            .cursor();
                        
                    }
                    else if (Number(queryInfo.index_start) >= 0){
                        
                        used_query_info.index_start = queryInfo.index_start;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                                                    .sort({serverTimestamp: 1})
                                                    .skip(Math.floor(Number(queryInfo.index_start)))
                            break;
                        }
                        return_data = Biometric_data.find({deviceInfo: device._id})
                                                    // .skip(Math.floor(Number(queryInfo.index_start)) + page_offset)
                                                    // // .limit(Math.floor(Number(queryInfo.get_length)))
                                                    // // .skip(page_offset)
                                                    // .limit(page_limit)
                                                    .sort({serverTimestamp: 1})
                                                    .skip(Math.floor(Number(queryInfo.index_start)))
                                                    .lean()
                                                    //.sort({_id: -1})
                                                    .cursor();
                    }
                    else if (Number(queryInfo.index_end) >= 0){
                        
                        used_query_info.index_end = queryInfo.index_end;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                                                    .sort({serverTimestamp: 1})
                                                    .limit(Math.floor(Number(queryInfo.index_end)))
                            break;
                        }

                        if (Math.floor(Number(queryInfo.index_end)) - page_offset < page_limit)
                            page_limit = Math.floor(Number(queryInfo.index_end)) - page_offset;
                        return_data = Biometric_data.find({deviceInfo: device._id})
                                                    // .skip(page_offset)
                                                    // // .sort({_id: -1})
                                                    // .limit(page_limit)
                                                    .sort({serverTimestamp: 1})
                                                    .limit(Math.floor(Number(queryInfo.index_end)))
                                                    .lean()
                                                    .cursor();
                    }
                    else if (Number(queryInfo.get_length) >= 0){
                        
                        used_query_info.get_length = queryInfo.get_length;

                        if (!get_data) {
                            count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                                                    .sort({serverTimestamp: 1})
                                                    .limit(Math.floor(Number(queryInfo.get_length)))
                                                    // .sort({_id: 1})
                            break;
                        }

                        if (Math.floor(Number(queryInfo.get_length)) - page_offset < page_limit)
                            page_limit = Math.floor(Number(queryInfo.get_length)) - page_offset;
                        return_data = Biometric_data.find({deviceInfo: device._id})
                                                    // .sort({_id: -1})
                                                    // .skip(page_offset)
                                                    // // .limit(Math.floor(Number(queryInfo.get_length)))
                                                    // // .sort({_id: 1})
                                                    // .limit(page_limit)
                                                    .sort({serverTimestamp: 1})
                                                    .limit(Math.floor(Number(queryInfo.get_length)))
                                                    .lean()
                                                    .cursor();
                    }
                    break;
                case 'get_latest':
                    
                    used_query_info.get_latest = queryInfo.get_latest;

                    if (!get_data) {
                        count_doc = await Biometric_data.countDocuments({deviceInfo: device._id})
                                                                .sort({serverTimestamp: -1})
                                                                .limit(1);
                        break;
                    }
                    return_data = Biometric_data.find({deviceInfo: device._id})
                                                        .sort({serverTimestamp: -1})
                                                        .limit(1)
                                                        .lean()
                                                        .cursor();
                    break;
                default:
                    break;
            }
            //logger.info(ret);
            // console.log(count_doc);
            ret = {
                return_data: return_data,
                query_info: used_query_info
            }
            if (!get_data){
                ret.count_doc = count_doc;
                // logger.info(ret);
            }
                
            // logger.info(count_doc);
            // logger.info(get_data);
            // logger.info(ret.count_doc);
            // console.log(ret);
            resolve(ret);
        }
        catch(err){
            reject(err);
        }
    })
}

module.exports = {
    initModel,
    queryDatabase,
    queryDatabaseStream,
    dropDeviceSensorData,
    setDeviceName,
    updateDeviceSensorData,
    createNewDevice,
    dropDevice,
    Biometric_data,
    Device_info
}