const winston = require('winston');
const prettyJson = winston.format.printf(info => {
    message = `${info.level}(${info.timestamp}): `;
    if (typeof info.message === 'object') {
        message += '\n' + JSON.stringify(info.message, null, 4).replace(/\\n/g, '\n');
    }
    else message += info.message;
    if (Object.keys(info.metadata).length) {
        message += '\n' + '>>>>>>>>>>>Metadata>>>>>>>>>>>>' + '\n' + JSON.stringify(info.metadata, null, 4).replace(/\\n/g, '\n') + '\n' + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>';
    }
    return message;
})

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.errors({ stack: true }),
        winston.format.metadata()
    ),
    transports: [
        new winston.transports.Console({
            level: 'debug',//process.env.LOG_LEVEL,
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json(),
                winston.format.splat(),
                //winston.format.prettyPrint(),
                
                winston.format.colorize(),
                prettyJson
            )
        })
    ]
});

process.on('unhandledRejection', (error) =>{
    logger.error(error);
});

process.on('uncaughtException', (error) =>{
    logger.error(error);
});

module.exports = logger;