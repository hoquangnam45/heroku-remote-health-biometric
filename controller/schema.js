const mongoose = require("mongoose");
var logger = require('./logger');
var email_checker = require('../controller/send_mail').checkEmail;
const ObjectId = mongoose.ObjectId;
const Schema = mongoose.Schema;


let deviceUuidValidator = (arg_uuid) => {
    return new Promise(async(resolve, reject) => {
        if (!arg_uuid) reject('Empty uuid');
        resolve(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(arg_uuid));
    });
};


var deviceInfoSchemaOpt = {}
var sensorRecordSchemaOpt = {timestamps: { createdAt: 'Created_at', updatedAt: 'Updated_at'}}
var userSchemaOpt = {}

var sensorRecordSchema = new Schema({
    deviceInfo: {
        type: ObjectId,
        ref:'remote-biometric-device',
        index: true
    },
    Red: Number,
    IR: Number,
    RedFil: Number,
    IRFil: Number,
    RedMA: Number,
    IRMA: Number,
    BPM: Number,
    spO2: Number,
    isBeat: Boolean,
    serverTimestamp: {
        type: Date,
        index: true,
        required: true
    },
    // originTimestamp: {type: Date, default: Date.now()},
    // cpuTimestamp: {type: Number, default: 0},
    // millis: {type: Number, default: 0}
    },
    // sensorRecordSchemaOpt
);

var deviceInfoSchema = new Schema({
    Device_uuid: {
        type: String,
        lowercase: true,
        required: true, 
        index: {unique: true, dropDups: true},
        validate: {
            validator: deviceUuidValidator,
            message: props => `${props.value} is not a valid device uuid!`
        }
    },
    Device_name: String,
    associatedUser: [
        {
            type:ObjectId,
            ref:'remote-biometric-user'
        }
    ],
    deviceInfoSchemaOpt
});


var userSchema = new mongoose.Schema({
    username: { 
        type: String, 
        required: true, 
        unique: true,
        index: {unique: true, dropDups: true},
    },
    email: { 
        type: String, 
        required: true, 
        unique: true,
        index: {unique: true, dropDups: true},
        validate: {
            validator: (arg_email) => {
                return new Promise(async(resolve, reject) => {
                    resolve(email_checker(arg_email));
                });
            },
            message: props => `${props.value} is not a valid email!`
        }
    },
    password: { type: String, required: true },
    associatedDevice: [
        {
            type:ObjectId,
            ref:'remote-biometric-device'
        }   
    ],
    resetPasswordToken: String,
    resetPasswordExpires: Date
    },
    userSchemaOpt
);

module.exports = {
    sensorRecordSchema,
    deviceInfoSchema,
    userSchema
};