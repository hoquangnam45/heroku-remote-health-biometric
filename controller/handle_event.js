const mongoose = require("mongoose");
var logger = require('./logger');

// Import events module
var notifier = require('./notifier');

const ROOM_NAME_PREFIX = "device room ";

module.exports = (io, mongodb_driver) => {
    
    // Broadcast to subcriber
    io.of('/subscribe_service').on('connection', socket => {
        var room_name;
        // logger.info('ff');
        socket.on('subscribe to', device_uuid => {
            if (room_name) socket.leave(room_name);
            room_name = ROOM_NAME_PREFIX + device_uuid;
            socket.join(room_name);
            logger.info('New subscribe to ' + room_name);
        });
        // socket.on('unsubcribe to', device_uuid => {
        //     if (room_name){
        //         socket.leave(room_name);
        //         logger.info('Subcriber leave ' + room_name);
        //         room_name = null;
        //     }
        // })
        let disconnectSocketFromRoom = () => {
            if (room_name){
                socket.leave(room_name);
                logger.info('Subcriber leave ' + room_name);
                room_name = null;
            }
        }
        socket.on('unsubcribe', disconnectSocketFromRoom);
        socket.on('disconnect', disconnectSocketFromRoom);
    })
    
    // Handling receive data
    io.on('connection', socket => {
        var new_session = false;
        logger.info('New client connected');
        // socket.on('ping', () => {});
        socket.on('test debug', (data) => {
            logger.info(data);
        })
        socket.on('push data', async (rawReceivedSensorData) => {
            try{
                // logger.info(receivedSensorData);
                // logger.info(receivedSensorData);
                ESP8266Array = JSON.parse(rawReceivedSensorData);
                let parseESP8266ArraytoObj = (ESP8266Array) => {
                    metadata = ESP8266Array[0];
                    ESP8266Obj = {};
                    ESP8266Obj["device_uuid"] = metadata[0];
                    ESP8266Obj["refServerTime"] = metadata[1];
                    ESP8266Obj["refCpuTime"] = metadata[2];
                    ESP8266Obj["payload"] = [];

                    refServerTimeMil = Date.parse(ESP8266Obj["refServerTime"]) 
                    if (isNaN(refServerTimeMil)) throw('No ref server timestamp');
                    for (i = 1; i < ESP8266Array.length; i++){
                        payloadArray = ESP8266Array[i];
                        payloadObj = {};
                        payloadObj["Red"] = payloadArray[0];
                        payloadObj["IR"] = payloadArray[1];
                        payloadObj["RedFil"] = payloadArray[2];
                        payloadObj["IRFil"] = payloadArray[3];
                        payloadObj["RedMA"] = payloadArray[4];
                        payloadObj["IRMA"] = payloadArray[5];
                        payloadObj["BPM"] = payloadArray[6];
                        payloadObj["spO2"] = payloadArray[7];
                        payloadObj["isBeat"] = payloadArray[8];
                        payloadObj["millis"] = payloadArray[9];
                        payloadObj["serverTimestamp"] = refServerTimeMil -
                                                        ESP8266Obj["refCpuTime"] +
                                                        payloadObj["millis"];
                        delete payloadObj["millis"];
                        ESP8266Obj["payload"].push(payloadObj);
                    }
                    return ESP8266Obj;
                }
                receivedSensorData = parseESP8266ArraytoObj(ESP8266Array);
                // logger.info(receivedSensorData);
                logger.info('device has new data ' + receivedSensorData.device_uuid);
                if (process.env.HANDLE_EVENT_DRY_RUN == 'false') dryRunFlag = false;
                else dryRunFlag = true;
                savedDoc = await mongodb_driver.sensordb_driver.updateDeviceSensorData(receivedSensorData, dryRunFlag); // This is async function
                //logger.info(args);
                //var doc = new mongoose.Document(args.payload, mongodb_driver.sensordb_driver.sensorRecordSchema);
                //let ret = await doc.validate();
                logger.info(new Date(savedDoc.insertedData[savedDoc.insertedData.length - 1].serverTimestamp).toISOString());
                new_session = false;
                notifier.emit('device has new data', receivedSensorData.device_uuid, receivedSensorData.payload); // Blocking function
            }
            catch(err){
                logger.info(err);
            }
        });
        
        // socket.on('push bulk data', async(args) => {
        //     await mongodb_driver.sensordb_driver.updateDeviceSensorData(args.device_uuid, args.payload); // This is async function
        //     //var doc = new mongoose.Document(args.payload, mongodb_driver.sensordb_driver.sensorRecordSchema);
        //     //let ret = await doc.validate();
        //     new_session = false;
        //     notifier.emit('device has new data', args.device_uuid, args.payload); // Blocking function
        // })
        socket.on('request time', async(args) => {
            var timestamp = new Date(Date.now());
            socket.emit('return time', timestamp.toISOString());
            logger.info(timestamp);
        })
        //attach event handlers
        socket.on('connect', () => {
            new_session = true;
            console.log('connect fired!');
        });
        socket.on('reconnect', () => {
            new_session = true;
            console.log('reconnect fired!');
        });
        socket.on('new session', () => {
            new_session = true;
        });
        socket.on('disconnect', reason => {
            logger.info(`reason: ${reason}`);
        });
    });

    // Bridging between device and subcriber
    notifier.on('device has new data', async (device_uuid, receivedSensorData) => {
        try{
            // At this step received sensor data must have been validated beforehand
            
            
            // logger.info(device_uuid);
            // logger.info(receivedSensorData);
            //console.log('ss');
            //console.log('dd' + io.sockets.adapter.rooms['device room ' + device_uuid]);
            await io.of('/subscribe_service').in(ROOM_NAME_PREFIX + device_uuid).emit('device has new data', device_uuid, receivedSensorData);
        }
        catch(err){
            logger.info(err);
        }
    });
}

// function subscribe(req, res) {

//     res.writeHead(200, {
//         'Content-Type': 'text/event-stream',
//         'Cache-Control': 'no-cache',
//         Connection: 'keep-alive'
//     });

//     // Heartbeat
//     const nln = function() {
//         res.write('\n');
//     };
//     const hbt = setInterval(nln, 15000);

//     const onEvent = function(data) {
//         res.write('retry: 500\n');
//         res.write(`event: event\n`);
//         res.write(`data: ${JSON.stringify(data)}\n\n`);
//     };

//     emitter.on('event', onEvent);

//     // Clear heartbeat and listener
//     req.on('close', function() {
//         clearInterval(hbt);
//         emitter.removeListener('event', onEvent);
//     });
// }

// function publish(eventData) {
//   // Emit events here recieved from Github/Twitter APIs
//     emitter.emit('event', eventData);
// }