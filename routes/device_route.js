var mongodb_driver = require('../controller/mongodb_driver')
var bodyParser = require("body-parser");
var express = require('express');
var router = express.Router();
var path = require('path');
var notifier = require('../controller/notifier');
var logger = require('../controller/logger');

router.get('/register_device', async (req, res) => {
    try{
        let device_uuid = req.query.device_uuid;
        let device_name = req.query.device_name;
        let ret = await mongodb_driver.sensordb_driver.createNewDevice(device_uuid, device_name)
        res.json({
            status: 'success',
            msg: ret
        });
        logger.info(ret);
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
        
    }
});

router.get('/set_device_name', async (req, res) => {
    try{
        let device_uuid = req.query.device_uuid;
        let device_name = req.query.device_name;
        let ret = await mongodb_driver.sensordb_driver.setDeviceName(device_uuid, device_name)
        res.json({
            status: 'success',
            msg: ret
        });
        logger.info(ret);
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.get('/push_data', async (req, res) => {
    try{
        let receivedSensorData = [
            {
                device_uuid: req.query.device_uuid,
                // refServerTime: req.query.refServerTime,
                // refCpuTime: req.query.refCpuTime,
                payload: [
                    {
                        Red: req.query.red,
                        IR: req.query.ir,
                        RedFil: req.query.redfil,
                        IRFil: req.query.irfil,
                        isBeat: req.query.isbeat,
                        RedMA: req.query.redma,
                        IRMA: req.query.irma,
                        BPM: req.query.bpm,
                        spO2: req.query.spO2,
                        isBeat: req.query.beatDetected,
                        serverTimestamp: req.query.serverTimestamp
                        // millis: req.query.millis
                    }
                ]
            }
        ]
        // logger.info(receivedSensorData);
        let ret = await mongodb_driver.sensordb_driver.updateDeviceSensorData(receivedSensorData);
        notifier.emit('device has new data', device_uuid, receivedSensorData);
        res.json({
            status: 'success',
            msg: ret.msg
        });
        logger.info(ret);
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
});

router.get('/drop_device', async (req, res) => {
    try{
        let device_uuid = req.query.device_uuid;
        let ret = await mongodb_driver.sensordb_driver.dropDevice(device_uuid);
        res.json({
            status: 'success',
            msg: ret
        });
        logger.info(ret);
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        });
        logger.info(err);
    }
})

router.get('/drop_sensor_data', async (req, res) => {
    try{
        let device_uuid = req.query.device_uuid;
        let ret = await mongodb_driver.sensordb_driver.dropDeviceSensorData(device_uuid);
        res.json({
            status: 'success',
            msg: ret
        });
        logger.info(ret);
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        });
        logger.info(err);
    }
})

router.get('/manage_device', async (req, res) => {
    res.render("manage_device");
})
module.exports = router;