var express = require('express');
var router = express.Router();
var passport = require('../controller/authenticateUser');
const logger = require("../controller/logger");
var mongodb_driver = require('../controller/mongodb_driver');
var mail_sender = require('../controller/send_mail');
const {cacher} = require("../controller/cacher");
const {CACHER_PREFIX} = require('../controller/cacher');
var JSONStream = require('JSONStream');
var stream = require('stream');
const {chain}  = require('stream-chain');
 
const {parser} = require('stream-json');
const { on } = require('../controller/logger');
const { Transform } = require('json2csv');
var path = require('path');

router.get('/', async (req, res) => {
    try{
        if (req.isAuthenticated()){
            let device = await mongodb_driver.userdb_driver.getDeviceInfo(req.user.associatedDevice);
            // logger.info(device);
            // logger.info('>>>>>>>>>>.')
            res.render('index', {user: req.user, SERVER_HOST: process.env.HEROKU_URL});
        }
        else {
            res.render('index', {user: null});
        }
    }
    catch(err){
        logger.info(err);
    }
})

// router.get('/:device_uuid', async (req, res) => {
//     try{
//         if (req.isAuthenticated()){
//             let device = await mongodb_driver.userdb_driver.getDeviceInfo(req.user.associatedDevice);
//             // logger.info(device);
//             // logger.info('>>>>>>>>>>.')
//             res.render('index', {user: req.user, active_device: device_uuid});
//         }
//         else {
//             res.render('index', {user: null});
//         }
//     }
//     catch(err){
//         logger.info(err);
//     }
// })

router.post('/addDevice', async(req, res) => {
    // logger.info(req.user);
    if (!req.isAuthenticated()){
        return res.status(500).json({
            status: 'fail',
            msg: 'Cant add device without login'
        })
    }    
    try{
        device_uuid = req.body.device_uuid;
        to_user = req.user;
        let ret = await mongodb_driver.userdb_driver.addDevice(to_user, device_uuid);
        let device_list = await mongodb_driver.userdb_driver.getDeviceInfo(ret.updated_user.associatedDevice);
        res.json({
            status: 'success',
            msg: ret.msg,
            updated_device_list: device_list
        });
    }
    catch (err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.post('/removeDevice', async(req, res) => {
    if (!req.isAuthenticated()){
        return res.status(500).json({
            status: 'fail',
            msg: 'Cant add device without login'
        })
    }
    try{
        device_uuid = req.body.device_uuid;
        from_user = req.user;
        let ret = await mongodb_driver.userdb_driver.removeDevice(from_user, device_uuid);
        let device_list = await mongodb_driver.userdb_driver.getDeviceInfo(ret.updated_user.associatedDevice);
        res.json({
            status: 'success',
            msg: ret.msg,
            updated_device_list: device_list
        });
    }
    catch (err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.post('/getDeviceInfo', async (req, res) => {
    try{
        if (req.isAuthenticated()){
            let device = await mongodb_driver.userdb_driver.getDeviceInfo(req.user.associatedDevice);
            // logger.info(device);
            // logger.info('>>>>>>>>>>.')
            res.json(device);
        }
        else {
            // logger.info('haha')
            res.json(null);
        }
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.get('/getData', async (req, res) => {
    if (!req.isAuthenticated()){
        return res.status(500).json({
            status: 'fail',
            msg: 'Cant get device data without login'
        })
    }
    try{
        var queryInfo = {
            Device_uuid: req.query.device_uuid,
            get_latest: req.query.get_latest,
            get_length: req.query.get_length,
            timerange_start: req.query.timerange_start,
            timerange_end: req.query.timerange_end
        }
        let ret = await mongodb_driver.sensordb_driver.queryDatabase(queryInfo);
        res.json({
            status: 'success',
            msg: 'Query successfully',
            query_info: queryInfo,
            return_data: ret
        })
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.post('/getDataStream', async (req, res) => {
    if (!req.isAuthenticated()){
        return res.status(500).json({
            status: 'fail',
            msg: 'Cant get device data without login'
        })
    }
    try{
        queryBody = req.body;
        var queryInfo = {
            Device_uuid: queryBody.device_uuid,
            get_latest: queryBody.get_latest,
            get_all: queryBody.get_all,
            timerange_start: queryBody.timerange_start,
            timerange_end: queryBody.timerange_end,
            index_start: queryBody.index_start,
            get_length: queryBody.get_length,
            index_end: queryBody.index_end,
            count_data: queryBody.count_data,
            offset: queryBody.offset,
            limit: queryBody.limit,
            // noOffsetLimit: true
        }
        let ret = await mongodb_driver.sensordb_driver.queryDatabaseStream(queryInfo);
        
        if (!(ret.return_data instanceof stream.Readable)){
            // logger.info(ret);
            return res.json({
                status: "success",
                msg: "Query successfully",
                query_info: ret.query_info,
                return_data: ret.return_data,
                count_doc: ret.count_doc
            });
        }    
        res.set('Content-Type', 'application/json');
        res.write(`{"status": "success", "msg": "Query successfully", "query_info": ` + JSON.stringify(ret.query_info) + `, "return_data": `);
        ret.return_data
        // .pipe(jsparser)
        .pipe(JSONStream.stringify())
        .on('end', () => {
            // console.log('done');
            res.write(`}`);
        })
        .pipe(res)


        // jsparser.on('data', function (obj) {
        //     console.log(obj); // whatever you will do with each JSON object
        // });
        // let obj = {
        //     status: 'success',
        //     msg: 'Query successfully',
        //     query_info: queryInfo,
        //     return_data: ret
        // };
        // ret.on('data',dat => {
        //     console.log(dat);
        //     next();
        // }).on('data', data=>{
        //     console.log('hihi');
        // })
        // const pipeline = chain([
        //     ret,
            
        //     data => {
        //         console.log(data)
                
        //     },
        //     JSONStream.stringify,
            
        // ])
        // pipeline.on('data', (data) => {
        //     console.log(data);
        // });
        // 
    }
    catch(err){
        logger.info(err);
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
    }
})

router.post('/downloadData', async (req, res) => {
    if (!req.isAuthenticated()){
        return res.status(500).json({
            status: 'fail',
            msg: 'Cant get device data without login'
        })
    }
    try{
        queryBody = req.body;
        var queryInfo = {
            Device_uuid: queryBody.device_uuid,
            get_latest: queryBody.get_latest,
            get_all: queryBody.get_all,
            timerange_start: queryBody.timerange_start,
            timerange_end: queryBody.timerange_end,
            index_start: queryBody.index_start,
            get_length: queryBody.get_length,
            index_end: queryBody.index_end,
            count_data: queryBody.count_data,
            offset: queryBody.offset,
            limit: queryBody.limit,
            noOffsetLimit: true
        }
        let ret = await mongodb_driver.sensordb_driver.queryDatabaseStream(queryInfo);
        
        if (!(ret.return_data instanceof stream.Readable)){
            // logger.info(ret);
            return res.json({
                status: "success",
                msg: "Query successfully",
                query_info: ret.query_info,
                return_data: ret.return_data,
                count_doc: ret.count_doc
            });
        }

        var headers = {
            'Content-Type': 'text/csv',
            'Content-disposition': 'attachment;filename=' + queryBody.device_uuid +'.csv',
        }
        const opts = {
            quote: '',
            delimiter: ';',
            fields: [
                'Red', 
                'IR', 
                {
                    label: 'Red Filtered',
                    value: 'RedFil'
                },
                {
                    label: 'IR Filtered',
                    value: 'IRFil'
                },
                {
                    label: 'Red Moving Average',
                    value: 'RedMA'
                },
                {
                    label: 'IR Moving Average',
                    value: 'IRMA'
                },
                'BPM', 
                'spO2',
                {
                    label: 'Measurement Timestamp',
                    value: (row, field) => {
                        return row.serverTimestamp;
                    }
                },
                {
                    label: 'Beat',
                    value: (row) => {
                        return row.isBeat;
                    }
                }
            ]
        };
        const transformOpts = {
            
        };
        const json2csv = new Transform(opts, transformOpts);

        res.writeHead(200, headers)
        
        ret.return_data
        // .pipe(jsparser)
        .pipe(JSONStream.stringify())
        .pipe(json2csv)
        .pipe(res)


        // jsparser.on('data', function (obj) {
        //     console.log(obj); // whatever you will do with each JSON object
        // });
        // let obj = {
        //     status: 'success',
        //     msg: 'Query successfully',
        //     query_info: queryInfo,
        //     return_data: ret
        // };
        // ret.on('data',dat => {
        //     console.log(dat);
        //     next();
        // }).on('data', data=>{
        //     console.log('hihi');
        // })
        // const pipeline = chain([
        //     ret,
            
        //     data => {
        //         console.log(data)
                
        //     },
        //     JSONStream.stringify,
            
        // ])
        // pipeline.on('data', (data) => {
        //     console.log(data);
        // });
        // 
    }
    catch(err){
        logger.info(err);
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
    }
})

router.get('/login', (req, res) => {
    if (req.isAuthenticated()) return res.redirect('/');
    res.render('login.pug');
})

router.post('/login', 
    (req, res, next) => {
        if (!req.body.login_info)
            return next("Empty name field")
        if (!req.body.password)
            return next("Empty password field");
        next();
    },
    (req, res, next) => {
        passport.authenticate(['login with email', 'login with username', 'login with login info'], function(err, user, info) {
            if (err) {
                logger.info(err);
                return next(err);
            }
            if (!user) {
                return res.status(500).json({msg : 'Wrong login info'});
            }
            req.logIn(user, function(err) {
                if (err) return next(err);
                return res.json({redirect : '/', msg: 'Log in successfully'});
            });
        })(req, res, next);
    }, (err, req, res, next) => {
        return res.status(500).json({msg: err});
    }
);

router.post('/register', 
    (req, res, next) => {
        if (!req.body.username)
            return next("Empty name field");
        if (!req.body.email)
            return next("Empty email field");
        if (!req.body.password)
            return next("Empty password field");
        next();
    },
    async (req, res, next) => {
        try{
            let userInfo = {
                username: req.body.username,
                email: req.body.email,
                password: req.body.password
            }
            let ret = await mongodb_driver.userdb_driver.registerUser(userInfo);
            logger.info("HHAA");
            req.logIn(ret.user, function(err) {
                if (err) logger.info(err);
                return res.json({redirect : '/', msg: 'Create user successfully'});
            });
        }
        catch (err){
            res.status(500).json({
                status: 'fail',
                msg: err,
                stack: err.stack
            })
            logger.info(err);
        }
    },
    (err, req, res, next) => {
        return res.status(500).json({msg: err});
    }
)

router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

router.post('/forgot_password', async(req, res) => {
    try{
        let ret = await mongodb_driver.userdb_driver.genResetPasswordToken(req.body.login_info);
        mail_sender.sendResetEmail(ret.email, req.headers.host, ret.token);
        res.json({msg: 'Reset email sent'});
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
});

router.get('/reset_password/:reset_token', async (req, res, next) => {
    try{
        let reset_token = req.params.reset_token;
        await mongodb_driver.userdb_driver.getUserWithResetToken(reset_token);
        res.render('reset_password');
    }
    catch(err){
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})

router.post('/reset_password/:reset_token', async (req, res) => {
    try{
        let reset_token = req.params.reset_token;
        let user = await mongodb_driver.userdb_driver.getUserWithResetToken(reset_token);
        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;
        await user.save();
        req.logIn(user, function(err) {
            if (err) throw(err);
            mail_sender.sendConfirmationReset(user);
            return res.json({redirect : '/', msg: 'Reset password successfully'});
        });
    }
    catch(err) {
        res.status(500).json({
            status: 'fail',
            msg: err,
            stack: err.stack
        })
        logger.info(err);
    }
})



module.exports = router;